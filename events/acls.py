from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json, requests

def get_picture_url(query):
    url = f"https://api.pexels.com/v1/search?query={query}"
    headers = {
            "Authorization": PEXELS_API_KEY
        }
    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict['photos'][0]['src']['original']

def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    tmp = json.loads(response.content)
    lat = tmp[0]["lat"]
    lon = tmp[0]["lon"]

    weatherurl = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    weather_response = requests.get(weatherurl)
    tmp2 = json.loads(weather_response.content)
    weather = {
        "weather": tmp2["weather"][0]["main"],
        "temp": tmp2["main"]["temp"],
    }
    return weather
